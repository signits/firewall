firewall
=========

This role starts and enables the firewalld service, according to the parameters provided.
It does not take any measures to allow or deny any inbound or outbound connections. Other roles may be responsible for this

Requirements
------------

None

Role Variables
--------------

enable_firewall: true - [controls if the module will run at all]

service_ensure: "started" - [ensures the firewall service runs]

service_enable: "yes" - [enables the firewall service]

firewall_deny_log: "all" - [whether to log all the packets that are rejected or dropped by firewall service]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
